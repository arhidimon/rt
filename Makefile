NOC=\033[0m
GRE=\033[32m
RED=\033[31m
YELL=\033[33m

CC := clang
PKGS := gtk+-3.0
SRCDIR := src
BUILDDIR := build
WARN=-Wall -Werror -Wextra
CFLAGS := $(WARN) -g  `pkg-config --cflags $(PKGS)`
LIBS := `pkg-config --libs $(PKGS)`
TARGET := RT
JSMN_DIR=jsmn
JSMN=jsmn.o
SRCEXT = c
SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
DEPS := $(OBJECTS:.o=.deps)

all: $(TARGET)


 $(TARGET) : $(OBJECTS) $(JSMN)
		@$(CC) $^ -o $(TARGET) $(LIBS)
		@echo "$(GRE)RT:\t\tRT READY$(GRE)"

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
		@mkdir -p $(BUILDDIR)
		@$(CC) $(CFLAGS) -c -o $@ $<
$(JSMN): %.o: $(JSMN_DIR)/%.$(SRCEXT)
		@$(CC) $(CFLAGS) -c -o $@ $<

clean:
		@$(RM) -r $(BUILDDIR) $(TARGET)
		@rm -f *.o $(TARGET) "jsmn.o"
		@echo "$(RED)RT:\t\tRemoving obj file$(RED)"

fclean:
	@rm -f *.o $(TARGET)
	@$(RM) -r $(BUILDDIR) $(TARGET) "jsmn.o"
	@echo "$(RED)RT:\t\tRemoving RT executables$(RED)"

re          : fclean all

-include $(DEPS)

.PHONY: clean

locale:
	export LC_ALL=en_US.UTF-8
	export LANG=en_US.UTF-8