/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_intersection.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vkaznodi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/14 15:41:34 by vkaznodi          #+#    #+#             */
/*   Updated: 2019/03/05 13:12:12 by atikhono         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

float			intersect_plane_b(t_primitive *primitive, t_ray *ray)
{
	t_vector	k;
	float		dot;
	t_vector	p;
	float		r[2];

	r[0] = primitive->p.plane_b.radius;
	dot = dotproduct(primitive->p.plane_b.normal, ray->direction);
	if (dot != 0)
	{
		k = primitive->p.plane_b.position - ray->position;
		r[1] = (dotproduct(k, primitive->p.plane_b.normal) / dot);
		p = ray->position + mult_vector(ray->direction, r[1]);
		if (p[0] > (primitive->p.plane_b.position[0] + r[0]) || p[0] <
			(primitive->p.plane_b.position[0] - r[0]) ||
			p[1] > (primitive->p.plane_b.position[1] + r[0]) || p[1] <
			(primitive->p.plane_b.position[1] - r[0]) ||
			p[2] > (primitive->p.plane_b.position[2] + r[0]) || p[2] <
			(primitive->p.plane_b.position[2] - r[0]))
			return (-1);
		return (r[1]);
	}
	return (-1);
}

t_vector		vect(t_primitive *primitive, t_vector p, int i)
{
	t_vector	p1;

	if (i == 1)
		return (vecros(primitive->p.trian.b - primitive->p.trian.a, p -
		primitive->p.trian.a));
	else if (i == 2)
		return (vecros(primitive->p.trian.c - primitive->p.trian.b, p -
		primitive->p.trian.b));
	else
	{
		return (vecros(primitive->p.trian.a - primitive->p.trian.c, p -
					primitive->p.trian.c));
	}
	p1[0] = 0;
	p1[1] = 0;
	p1[2] = 0;
	return (p1);
}

float			intersect_trian(t_primitive *primitive, t_ray *ray)
{
	t_vector	c[6];
	float		test[2];
	t_vector	k;
	t_vector	h;

	position_tri(primitive);
	c[0] = primitive->p.trian.b - primitive->p.trian.a;
	c[1] = primitive->p.trian.c - primitive->p.trian.b;
	c[2] = primitive->p.trian.a - primitive->p.trian.c;
	k = ray->position - primitive->p.trian.a;
	if ((test[1] = dotproduct(ray->direction,
			primitive->p.trian.normal)) == 0.f)
		return (INFINITY);
	test[0] = -dotproduct(k, primitive->p.trian.normal) / test[1];
	h = ray->position + mult_vector(ray->direction, test[0]);
	c[3] = h - primitive->p.trian.a;
	c[4] = h - primitive->p.trian.b;
	c[5] = h - primitive->p.trian.c;
	if ((dotproduct(primitive->p.trian.normal, vecros(c[0], c[3]))) < 0)
		return (INFINITY);
	if ((dotproduct(primitive->p.trian.normal, vecros(c[1], c[4]))) < 0)
		return (INFINITY);
	if ((dotproduct(primitive->p.trian.normal, vecros(c[2], c[5]))) < 0)
		return (INFINITY);
	return (test[0]);
}

float			intersect_box(t_primitive *primitive, t_ray *ray)
{
	t_vector	dirfrac;
	float		t[6];
	t_roots		t1;

	position_box(primitive);
	dirfrac[0] = (1 / ray->direction[0]);
	dirfrac[1] = (1 / ray->direction[1]);
	dirfrac[2] = (1 / ray->direction[2]);
	t[0] = (primitive->p.box.a[0] - ray->position[0]) * dirfrac[0];
	t[1] = (primitive->p.box.b[0] - ray->position[0]) * dirfrac[0];
	t[2] = (primitive->p.box.a[1] - ray->position[1]) * dirfrac[1];
	t[3] = (primitive->p.box.b[1] - ray->position[1]) * dirfrac[1];
	t[4] = (primitive->p.box.a[2] - ray->position[2]) * dirfrac[2];
	t[5] = (primitive->p.box.b[2] - ray->position[2]) * dirfrac[2];
	t1[0] = max(max(min(min(t[0], t[1]), INFINITY), min(min(t[2], t[3]),
		INFINITY)), min(min(t[4], t[5]), INFINITY));
	t1[1] = min(min(max(max(t[0], t[1]), -INFINITY), max(max(t[2], t[3]),
		-INFINITY)), max(max(t[4], t[5]), -INFINITY));
	if (t1[1] < 1e-8 || t1[0] >= t1[1] || t1[0] < 1e-8)
		return (INFINITY);
	return ((t1[0] >= 0) ? t1[0] : t1[1]);
}

float			intersect_disk(t_primitive *primitive, t_ray *ray)
{
	float		t;
	float		d2;
	float		dot;
	t_vector	k[3];

	dot = dotproduct(primitive->p.disk.normal, ray->direction);
	if (dot != 0)
	{
		k[0] = primitive->p.disk.position - ray->position;
		t = dotproduct(k[0], primitive->p.disk.normal) / dot;
		if (t < INFINITY)
		{
			k[1] = ray->position + mult_vector(ray->direction, t);
			k[2] = k[1] - primitive->p.disk.position;
			d2 = sqrtf(dotproduct(k[2], k[2]));
			if (d2 <= primitive->p.disk.radius)
				return (d2);
		}
	}
	return (INFINITY);
}
