/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scenes.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbezruch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/30 13:30:32 by dbezruch          #+#    #+#             */
/*   Updated: 2019/01/17 19:34:20 by atikhono         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

void		box(t_vector pos, float r)
{
	t_primitive *p;

	p = add_plane_b(A_PR, (t_vector) {pos[0] - r, pos[1], pos[2]},
		(t_vector){-1, 0, 0}, r);
	p->reflection = 0.0;
	p = add_plane_b(A_PR, (t_vector) {pos[0] + r, pos[1], pos[2]},
		(t_vector){1, 0, 0}, r);
	p->reflection = 0.0;
	p = add_plane_b(A_PR, (t_vector) {pos[0], pos[1] - r, pos[2]},
		(t_vector){0, -1, 0}, r);
	p->reflection = 0.0;
	p = add_plane_b(A_PR, (t_vector) {pos[0], pos[1] + r, pos[2]},
		(t_vector){0, 1, 0}, r);
	p->reflection = 0.0;
	p = add_plane_b(A_PR, (t_vector) {pos[0], pos[1], pos[2] + r},
		(t_vector){0, 0, 1}, r);
	p->reflection = 0.0;
	p = add_plane_b(A_PR, (t_vector) {pos[0], pos[1], pos[2] - r},
		(t_vector){0, 0, -1}, r);
	p->reflection = 0.0;
}

void		next2(t_primitive *p, t_vector pos)
{
	p = add_sphere(A_PR, (t_vector) {pos[0] + 0.2, pos[1] + 2.7, pos[2] + -0.8},
		0.1, 0xF0FF00);
	p->reflection = 0.0;
	p->specular = 50;
	p = add_sphere(A_PR, (t_vector) {pos[0] + 0, pos[1] + 0, pos[2] + -1.7},
		0.1, 0xF0FF00);
	p->reflection = 0.0;
	p->specular = 100;
}

void		next(t_primitive *p, t_vector pos)
{
	t_vector asd;

	p = add_sphere(A_PR, (t_vector) {pos[0] + 0, pos[1] + 0.5, pos[2] + -1.6},
		0.1, 0xF0FF00);
	p->reflection = 0.0;
	p->specular = 10;
	p = add_sphere(A_PR, (t_vector) {pos[0] + 0, pos[1] + -1.5, pos[2] + -1.75},
		0.1, 0xF0FF00);
	p->reflection = 0.0;
	p->specular = 50;
	p = add_sphere(A_PR, (t_vector) {pos[0] + 0, pos[1] + 2.5, pos[2] + -0.9},
		0.1, 0xF0FF00);
	p->reflection = 0.0;
	p->specular = 100;
	p = add_sphere(A_PR, (t_vector) {pos[0] + 0, pos[1] + -2.5, pos[2] + -2.2},
		0.1, 0xF0FF00);
	p->reflection = 0.0;
	p->specular = 50;
	p = add_sphere(A_PR, (t_vector) {pos[0] + 0, pos[1] + -3.5, pos[2] + -2.3},
		0.1, 0xF0FF00);
	p->reflection = 0.0;
	p->specular = 100;
	asd = (t_vector) {pos[0] + 0, pos[1] + 4, pos[2] + 0.2};
	next2(p, pos);
	box(asd, 0.7);
}

void		testscene_snowman(t_vector pos)
{
	t_primitive	*p;

	g_app->scene.primitives = NULL;
	g_app->scene.lights = NULL;
	add_directional_light(&(g_app->scene.lights), (t_vector) {0, 2, -3}, 0.9);
	add_ambient_light(&(g_app->scene.lights), 0.4);
	p = add_sphere(A_PR, (t_vector) {pos[0] + 0, pos[1] + -3.2, pos[2] + 0.7},
		3, 0xFFFFFF);
	p->reflection = 0.0;
	p->specular = 10;
	p = add_sphere(A_PR, (t_vector) {pos[0] + 0, pos[1] + 0, pos[2] + 0.3}, 2,
		0xFFFFFF);
	p->reflection = 0.0;
	p->specular = 50;
	p = add_sphere(A_PR, (t_vector) {pos[0] + 0, pos[1] + 2.5, pos[2] + 0.1}, 1,
		0xFFFFFF);
	p->reflection = 0.0;
	p->specular = 100;
	p = add_sphere(A_PR, (t_vector) {pos[0] + -0.2, pos[1] + 2.7, pos[2] + -0.8}
		, 0.1, 0xF0FF00);
	p->reflection = 0.0;
	p->specular = 10;
	next(p, pos);
}
