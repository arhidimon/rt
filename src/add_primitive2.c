/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_primitive2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vkaznodi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/17 18:34:23 by vkaznodi          #+#    #+#             */
/*   Updated: 2019/01/17 19:31:25 by atikhono         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

t_primitive		*add_trian(t_primitive **primitives, t_vector position,
		float radius, int color)
{
	t_primitive *primitive;

	primitive = add_primitive(primitives);
	primitive->rotation = (t_vector){0, 1, 0};
	primitive->type = TRIAN;
	primitive->p.trian.position = position;
	primitive->p.trian.normal = (t_vector){0, 0, 1};
	primitive->p.trian.radius = radius;
	primitive->p.trian.radius2 = radius * radius;
	primitive->reflection = 0.0;
	primitive->specular = -1;
	primitive->color = color;
	return (primitive);
}

t_primitive		*add_box(t_primitive **primitives, t_vector position,
		float radius, int color)
{
	t_primitive *primitive;

	primitive = add_primitive(primitives);
	primitive->rotation = (t_vector){0, 1, 0};
	primitive->type = BOX;
	primitive->p.box.position = position;
	primitive->p.box.radius = radius;
	primitive->p.box.radius2 = radius * radius;
	primitive->reflection = 0.0;
	primitive->specular = -1;
	primitive->color = color;
	return (primitive);
}

int				side(t_vector normal)
{
	if (normal[0] == 0 && normal[1] == 0 && normal[2] == -1)
		return (1);
	else if (normal[0] == 0 && normal[1] == 0 && normal[2] == 1)
		return (2);
	else if (normal[0] == -1 && normal[1] == 0 && normal[2] == 0)
		return (3);
	else if (normal[0] == 1 && normal[1] == 0 && normal[2] == 0)
		return (4);
	else if (normal[0] == 0 && normal[1] == 1 && normal[2] == 0)
		return (5);
	else if (normal[0] == 0 && normal[1] == -1 && normal[2] == 0)
		return (6);
	return (0);
}

t_primitive		*add_plane_b(t_primitive **primitives, t_vector position,
		t_vector normal, float r)
{
	t_primitive *primitive;

	primitive = add_primitive(primitives);
	primitive->rotation = (t_vector){0, 0, 0};
	primitive->type = PLANE_B;
	primitive->p.plane_b.position = position;
	primitive->p.plane_b.normal = normal;
	primitive->p.plane_b.radius = r;
	primitive->p.plane_b.pos = side(primitive->p.plane_b.normal);
	primitive->reflection = 0.0;
	primitive->specular = -1;
	primitive->color = color_side(primitive->p.plane_b.pos);
	return (primitive);
}

t_primitive		*add_disk(t_primitive **primitives, t_vector position,
		t_vector normal, int color)
{
	t_primitive *primitive;

	primitive = add_primitive(primitives);
	primitive->rotation = (t_vector){0, 0, 0};
	primitive->type = DISK;
	primitive->p.disk.position = position;
	primitive->p.disk.normal = normal;
	primitive->p.disk.radius = 4;
	primitive->reflection = 0.0;
	primitive->specular = -1;
	primitive->color = color;
	return (primitive);
}
