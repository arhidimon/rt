/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   posit.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vkaznodi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/05 12:41:23 by vkaznodi          #+#    #+#             */
/*   Updated: 2019/03/05 12:55:48 by atikhono         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

void		position_tri(t_primitive *primitive)
{
	primitive->p.trian.a = (t_vector){(primitive->p.trian.position[0] -
		primitive->p.trian.radius2), (primitive->p.trian.position[1] -
		primitive->p.trian.radius2), primitive->p.trian.position[2] + 0.0001};
	primitive->p.trian.b = (t_vector){(primitive->p.trian.position[0] +
		primitive->p.trian.radius2), (primitive->p.trian.position[1] -
		primitive->p.trian.radius2), primitive->p.trian.position[2] + 0.0001};
	primitive->p.trian.c = (t_vector){primitive->p.trian.position[0],
		(primitive->p.trian.position[1] + primitive->p.trian.radius2),
		primitive->p.trian.position[2] + 0.0001};
}

void		position_box(t_primitive *primitive)
{
	primitive->p.box.a = (t_vector){(primitive->p.box.position[0]),
		(primitive->p.box.position[1]), primitive->p.box.position[2]};
	primitive->p.box.b = (t_vector){(primitive->p.box.position[0] +
		primitive->p.box.radius2), (primitive->p.box.position[1] +
		primitive->p.box.radius2), primitive->p.box.position[2] +
		primitive->p.box.radius2};
}
