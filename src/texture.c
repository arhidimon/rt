/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atikhono <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/05 12:33:47 by atikhono          #+#    #+#             */
/*   Updated: 2019/03/05 14:02:15 by atikhono         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

int			get_text_color(int x, int y)
{
	int		color;

	color = (int)(g_app->raw_text[y * g_app->text_w * 3 + x * 3 + 2]);
	color <<= 8;
	color += (int)(g_app->raw_text[y * g_app->text_w * 3 + x * 3 + 1]);
	color <<= 8;
	color += (int)(g_app->raw_text[y * g_app->text_w * 3 + x * 3]);
	return (color);
}

/*
**make keys for for textures
*/

/*
**rowstride = gdk_pixbuf_get_rowstride(pixbuf);
**pixels = gdk_pixbuf_get_pixels(pixbuf);
**(int)pixels[y * rowstride + x * 3]
*/
int			get_text_sphere(t_vector norm)
{
	float		u;
	float		v;
	int			x;
	int			y;

	u = 0.5 + -atan2(norm[0], norm[2]) / (2 * M_PI);
	v = 0.5 + -norm[1] * 0.5;
	x = roundf(u * g_app->text_w);
	y = roundf(v * g_app->text_h);
	return (get_text_color(x, y));
}

int			get_text_cyli(t_vector norm)
{
	float		u;
	float		v;
	int			x;
	int			y;

	u = 0.5 + -atan2(norm[0], norm[2]) / (2 * M_PI);
	v = 0.5 + norm[1] * 0.5;
	x = (int)(roundf(u * g_app->text_w)) % g_app->text_w;
	y = abs((int)(roundf(v * g_app->text_h))) % g_app->text_h;
	if (v >= 0.0001)
		y = g_app->text_h - y - 1;
	return (get_text_color(x, y));
}

int			get_text_plane(t_vector norm)
{
	float		u;
	float		v;
	int			x;
	int			y;

	u = -norm[0];
	v = norm[2];
	x = abs((int)(roundf(u * g_app->text_w))) % g_app->text_w;
	y = abs((int)(roundf(v * g_app->text_h))) % g_app->text_h;
	if (u >= 0.0001)
		x = g_app->text_w - x - 1;
	if (v >= 0.0001)
		y = g_app->text_h - y - 1;
	return (get_text_color(x, y));
}

int			get_text(t_primitive *obj, t_ray *ray, float clos_t)
{
	t_vector	norm;

	if (g_app->raw_text != NULL && g_app->text_on)
	{
		if (obj->type == SPHERE || obj->type == CONE)
		{
			norm = normalize(g_app->tempvector);
			return (get_text_sphere(norm));
		}
		else if (obj->type == CYLINDER)
		{
			norm = (get_normal(obj, ray, clos_t));
			return (get_text_cyli(norm));
		}
		else if (obj->type == PLANE || obj->type == PLANE_B)
		{
			norm = ray->position + mult_vector((ray->direction), clos_t)\
				+ obj->p.plane.normal;
			return (get_text_plane(norm));
		}
	}
	return (obj->color);
}
